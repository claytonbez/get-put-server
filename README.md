## get-put-server ##
*version 0.0.1a*

A simple bare bones file repository for local networks. 

### Installation

Clone from bitbucket

    git clone git clone https://claytonbez@bitbucket.org/claytonbez/get-put-server.git

Move files from windows/linux directory to your local drive. Add application to ENVIRONMENT VARIABLES to use/ or run native from directory

    gp-server --port 54000
	-OR-
	gp-server.exe --port 54000

### Your repo server is ready!

use the get-put repo for the client that gets and puts directories to the server.

get-put client -> [get-put on BitBucket](https://bitbucket.org/claytonbez/get-put/src/master/ "get-put")